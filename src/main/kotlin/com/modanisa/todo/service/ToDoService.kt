package com.modanisa.todo.service

import com.modanisa.todo.domain.ToDo
import com.modanisa.todo.domain.dto.CreateToDoRequest
import com.modanisa.todo.repository.ToDoRepository
import org.springframework.stereotype.Service

@Service
class ToDoService(
        private val toDoRepository: ToDoRepository
) {

    fun createToDo(createToDoRequest: CreateToDoRequest): ToDo {
        val newToDo = ToDo(text = createToDoRequest.text)
        return toDoRepository.save(newToDo)
    }

    fun getToDos(): List<ToDo> {
        return toDoRepository.findAll()
    }
}