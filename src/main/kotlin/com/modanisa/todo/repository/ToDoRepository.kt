package com.modanisa.todo.repository

import com.modanisa.todo.domain.ToDo
import org.springframework.data.jpa.repository.JpaRepository

interface ToDoRepository : JpaRepository<ToDo, Long>