package com.modanisa.todo.controller

import com.modanisa.todo.domain.ToDo
import com.modanisa.todo.domain.dto.CreateToDoRequest
import com.modanisa.todo.service.ToDoService
import org.springframework.web.bind.annotation.*

@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
@RestController
@RequestMapping("/todos")
class ToDoController(
        private val toDoService: ToDoService
) {

    @PostMapping
    fun createToDo(@RequestBody createToDoRequest: CreateToDoRequest): ToDo {
        return toDoService.createToDo(createToDoRequest)
    }

    @GetMapping
    fun getToDos(): List<ToDo> {
        return toDoService.getToDos()
    }
}