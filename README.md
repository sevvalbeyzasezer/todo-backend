# modanisa-todo-backend

## Project description
This is a simple ToDo list application which holds added ToDo's on H2 temporary database  

## Deployment to cloud
```
gradlew bootBuildImage --imageName=gcr.io/red-planet-312108/modanisa-todo-backend:0.0.1
docker push gcr.io/red-planet-312108/modanisa-todo-backend:0.0.1
```

## Run docker image on local
```
docker run -p 8081:8081 --rm --name modanisa-todo-backend gcr.io/red-planet-312108/modanisa-todo-backend:0.0.1
```
