package com.modanisa.todo.domain.dto

class CreateToDoRequest(
        val text: String
)
