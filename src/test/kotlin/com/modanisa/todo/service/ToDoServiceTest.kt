package com.modanisa.todo.service

import com.modanisa.todo.domain.ToDo
import com.modanisa.todo.domain.dto.CreateToDoRequest
import com.modanisa.todo.repository.ToDoRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ToDoServiceTest {

    private val toDoRepository = mockk<ToDoRepository>()
    private val toDoService = ToDoService(toDoRepository)

    @Test
    fun createTodoTest() {
        // Given
        val createToDoRequest = CreateToDoRequest("yoga")
        val newToDo = ToDo(text = createToDoRequest.text)
        val expected = ToDo(1, createToDoRequest.text)

        every { toDoRepository.save(newToDo) } returns expected

        // When
        val actual = toDoService.createToDo(createToDoRequest)

        // Then
        assertEquals(expected, actual)
    }

    @Test
    fun getToDos() {
        // Given
        val todo1 = ToDo(1, "yoga")
        val todo2 = ToDo(2, "yoga")
        val expected = listOf(todo1, todo2)

        every { toDoRepository.findAll() } returns expected

        // When
        val actual = toDoService.getToDos()

        // Then
        assertEquals(expected, actual)
    }
}